<div class="modal fade" id="inputDataModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form id="dataInput" class="form-horizontal">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Data Baru</h4>
      </div>
      <div class="modal-body">
        
            <div class="box-body">

            <div class="form-group">
                <label class="col-sm-3 control-label">Jumlah Populasi</label>
                <div class="col-sm-9">
                <input type="text" name="populasi" class="form-control"  placeholder="Jumlah populasi yang diinginkan" value="4">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Iterasi</label>
                <div class="col-sm-9">
                <input type="text" name="iterasi" class="form-control"  placeholder="Masukkan iterasi yang diinginkan" value="2">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Crossover Rate</label>
                <div class="col-sm-9">
                <input type="text" name="cr" class="form-control"  placeholder="Masukkan crossover rate" value="0.8">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Mutation Rate</label>
                <div class="col-sm-9">
                <input type="text" name="mr" class="form-control"  placeholder="Masukkan mutation rate" value="0.6">
                </div>
            </div>

            
            </div>
            <!-- /.box-body -->
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="lanjutkanButton">Lanjutkan</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="inputDataKonfirmasiModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <form class="form-horizontal" id="dataInputFinal" method="POST" action="<?php echo base_url('data/hitung') ?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning">Cek inputan Anda sebelum melanjutkan perhitungan. Silahkan kembali apabila ada kesalahan input</div>
        <div class="row invoice-info">
            <div class="col-sm-3 invoice-col">
            Jumlah Populasi
            <!-- <address>
                <strong id="populasiValue">2</strong>
            </address> -->
            <input type="text" class="form-control" id="populasiCount" name="populasiCount" readonly/>
            </div>
            <div class="col-sm-3 invoice-col">
            Iterasi
            <!-- <address>
                <strong id="iterasiValue">2</strong>
            </address> -->
            <input type="text" class="form-control" id="iterasiValue" name="iterasiValue" readonly/>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
            Crossover Rate
            <!-- <address>
                <strong id="crValue">0.7</strong>
            </address> -->
            <input type="text" class="form-control" id="crValue" name="crValue" readonly/>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
            Mutation Rate
            <!-- <address>
                <strong id="mrValue">0.3</strong>
            </address> -->
            <input type="text" class="form-control" id="mrValue" name="mrValue" readonly/>
            </div>
            <!-- /.col -->
        </div>
        <div id="tabelPopulasiFieldHidden"></div>
        <table id="tabelPopulasi" class="table table-striped dataTable">
            <thead>
            <tr>
              <th>Populasi</th>
              <th>Gen</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-toggle="modal" data-target="#inputDataModal" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->