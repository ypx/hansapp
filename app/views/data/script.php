<script>

window.onload = function(){

    $("#lanjutkanButton").attr("disabled", "disabled");

    var isLanjutkanButtonAktif = false;

    var max_populasi = 999;
    var min_populasi = 2;

    var max_iterasi = 999;
    var min_iterasi = 1;

    var max_rate = 1;
    var min_rate = 0.1;

    $("#dataInput input").keyup(function(){
        
        var populasiInput = $("input[name=populasi]").val();
        var iterasiInput = $("input[name=iterasi]").val();
        var crInput = $("input[name=cr]").val();
        var mrInput = $("input[name=mr]").val();

        if( 
            (populasiInput >= min_populasi && populasiInput <= max_populasi) &&
            (iterasiInput >= min_iterasi && iterasiInput <= max_iterasi) &&
            (crInput >= min_rate && crInput <= max_rate) &&
            (mrInput >= min_rate && mrInput <= max_rate)
        ) {
            isLanjutkanButtonAktif = true;
        } else {
            isLanjutkanButtonAktif = false;
        }

        console.log(isLanjutkanButtonAktif);

        if(isLanjutkanButtonAktif == true) {
            $("#lanjutkanButton").removeAttr("disabled");
        } else {
            $("#lanjutkanButton").attr("disabled", "disabled");
        }
    });

    $("#dataInput").submit(function(e){
        var formData = $(this).serializeArray();
        var populasiData = formData[0].value;
        var iterasi = formData[1].value;
        var cr = formData[2].value;
        var mr = formData[3].value;

        $('#inputDataModal').modal('hide');
        $("#inputDataKonfirmasiModal").modal('show');

        $("#populasiCount").val(populasiData);
        $("#iterasiValue").val(iterasi);
        $("#crValue").val(cr);
        $("#mrValue").val(mr);

        $.getJSON("<?php echo base_url('data/generateRandomGen') ?>/" + populasiData + "/19", function(data){
            var dataSet = data.data;
            console.log(dataSet);
            $.each(dataSet, function(index, dataSingleGen){
                var dataSingleGenID = dataSingleGen[0];
                var dataSingleGenValue = dataSingleGen[1];
                $("#tabelPopulasiFieldHidden").append("<input type='hidden' value='"+dataSingleGenValue+"' name='"+dataSingleGenID+"'/>");
            });
            $('#tabelPopulasi').DataTable({
                data: dataSet,
                columns: [
                    { title: "PX" },
                    { title: "GEN" }
                ],
                "aLengthMenu": [[5, 10, -1], [5, 10, "All"]]
            });
                // console.log(dataSet);
        });
        e.preventDefault();

    })

    // $("#dataInputFinal").submit(function(e){
    //     var formData = $(this).serializeArray();
    //     console.log(formData);
    //     e.preventDefault();

    // })
}

</script>