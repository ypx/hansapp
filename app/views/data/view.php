<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Perhitungan</h3>
                <div class="box-tools">
                    <a data-toggle="modal" data-target="#inputDataModal" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i> Tambah Data
                    </a>
                </div>
            </div> <!-- /.box-header -->
            <div class="box-body">
              <?php $this->load->view('snippets/notification') ?>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#seleksi" data-toggle="tab">Hasil Seleksi</a></li>
                  <li><a href="#crossover" data-toggle="tab">Crossover</a></li>
                  <li><a href="#mutasi" data-toggle="tab">Mutasi</a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="seleksi">
                    <?php echo $this->session->flashdata('seleksi_table') ?>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="crossover">
                    <?php echo $this->session->flashdata('crossover_table') ?>
                  </div>
                  <div class="tab-pane" id="mutasi">
                    <?php echo $this->session->flashdata('mutasi_table') ?>
                  </div>
                </div>
                <!-- /.tab-content -->
              </div>
            </div> <!-- /.box-body -->
        </div> <!-- /.box -->
    </div> <!-- /.col -->
</div>

<?php $this->load->view('data/modal') ?>
<?php $this->load->view('data/script') ?>