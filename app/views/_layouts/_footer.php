<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        We fall in love with people that we can't have <i class="fa fa-heart" style="color: red"></i>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Digado</a>.</strong> All rights reserved.
</footer>

