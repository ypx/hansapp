<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo vendor_url('almasaeed2010/adminlte') ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo vendor_url('almasaeed2010/adminlte') ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo vendor_url('almasaeed2010/adminlte') ?>/plugins/select2/select2.full.min.js"></script>

<!-- Datatables -->
<script src="<?php echo vendor_url('almasaeed2010/adminlte') ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo vendor_url('almasaeed2010/adminlte') ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo vendor_url('almasaeed2010/adminlte') ?>/dist/js/app.min.js"></script>


<!-- bootstrap datepicker -->
<script src="<?php echo vendor_url('almasaeed2010/adminlte') ?>/plugins/datepicker/bootstrap-datepicker.js"></script>

<!-- MAPS -->


<script type="text/javascript">

    
    $(document).ready(function(){

        // Select2
        $('.select2').select2();

        // Datepicker
	    $('.datepicker').datepicker({
	       autoclose: true
	    });

        // Datatables
        // $('.dataTable').DataTable({
        //     "aLengthMenu": [[5, 10, -1], [5, 10, "All"]]
        // });

        // Set class to current active menu
        $("ul.sidebar-menu>li>ul").find('li.active').parent().parent().addClass('active');

        $('#email').on('change', function () {
           var email = $('#email') .val();
           $('#notif_email').html('Checking Email... Tunggu sebentar... ');
           $.ajax({
                type: 'POST',
                url: "<?=site_url($this->cur.'/cekEmail')?>",
                data: {email: email}
           }).done(function (hasil) {
               $('#notif_email').html(hasil);
           })
        });

    })
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout.