<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo default_img('avatar') ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>
            <?php echo $this->session->userdata('name'); ?>
            {session_name}
          </p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->

      <?php //print_r($this->session->all_userdata()); ?>
      
      <?php

      $menuData = array(
          'dashboard' => array(
              'url' => 'dashboard', 'icon' => 'dashboard', 'status' => 'WIP'
            ),
          'data' => array(
              'url' => 'data', 'icon' => 'gear', 'status' => 'DONE'
            ),
          'keluar' => array(
              'url' => 'logout', 'icon' => 'sign-out', 'status' => 'DONE'
            )
        );

      ?>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header"><?php //echo ucwords(ENV) ?> Version</li>

        <?php foreach($menuData as $key => $menu) : ?>
        
        <li class="
            <?php 
            if($menu['url'] == @$this->cur) : 
              echo 'active';
            endif;
            ?>
            ">
          <a href="<?php echo base_url($menu['url']) ?>"><i class="fa fa-<?php echo $menu['icon'] ?>"></i> 
            <span>
              <?php echo ucwords($key) ?>
            </span>
              <?php if(isset($menu['child'])): ?>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
              <?php endif; ?>
          </a>
          <?php if(isset($menu['child'])): ?>
          <ul class="treeview-menu">
            <?php foreach($menu['child'] as $subkey => $submenu): ?>
            <li class="
            <?php 
            if($submenu['url'] == @$this->cur) : 
              echo 'active';
            endif;
            ?>
            ">
              <a href="<?php echo base_url($submenu['url']) ?>">
                <i class="fa fa-<?php echo $submenu['icon'] ?>"></i> 
                  <?php echo ucwords($subkey) ?>
              </a>
            </li>
            <?php endforeach; ?>
          </ul>
          <?php endif; ?>
        </li>
        
        <?php endforeach; ?>        
        
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
