<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="<?php echo vendor_url('almasaeed2010/adminlte') ?>/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo vendor_url('fortawesome/font-awesome') ?>/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="<?php echo vendor_url('driftyco/ionicons') ?>/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo vendor_url('almasaeed2010/adminlte') ?>/plugins/select2/select2.min.css">

<link rel="stylesheet" href="<?php echo vendor_url('almasaeed2010/adminlte') ?>/plugins/datatables/dataTables.bootstrap.css">

<link rel="stylesheet" href="<?php echo vendor_url('almasaeed2010/adminlte') ?>/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
      page. However, you can choose any other skin. Make sure you
      apply the skin class to the body tag so the changes take effect.
-->
<link rel="stylesheet" href="<?php echo vendor_url('almasaeed2010/adminlte') ?>/dist/css/skins/skin-blue.min.css">

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?php echo vendor_url('almasaeed2010/adminlte') ?>/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo vendor_url('almasaeed2010/adminlte') ?>/plugins/datepicker/datepicker3.css">


<link rel="stylesheet" href="<?php echo assets_url('css', 'style.css') ?>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
