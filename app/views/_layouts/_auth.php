<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $sitename ?> | Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <?php $this->load->view('_layouts/_css') ?>

  <style>
    body {
      /*background-image: url(https://unsplash.it/1600/900) no-repeat !important;*/
      background: url(<?php echo assets_url('img', 'bg-login.jpg') ?>) !important; 
      background-repeat: no-repeat !important;
      background-position: center center !important;
      background-size: cover !important;
      position: relative !important;
    }
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="">Digado Admin</a>
  </div>
  <?=@$_body?>
</div>
<!-- /.login-box -->

<?php $this->load->view('_layouts/_js'); ?>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
