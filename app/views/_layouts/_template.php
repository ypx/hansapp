<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<!-- template -->

<!-- include head.php here -->

<?php echo @$_head ?>
<!-- include sidebar.php here -->
<?php echo @$_menu ?>
<!-- include body.php here -->

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo @$page_title ?>
        <small><?php echo @$subpage_title ?></small>
      </h1>
      <!-- <ol class="breadcrumb">
        <i class="fa fa-dashboard"></i> <?php echo set_breadcrumb(); ?> 
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo @$_body ?>
      <!-- Your Page Content Here -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- include foot.php here -->
<?php echo @$_foot ?>

</html>

