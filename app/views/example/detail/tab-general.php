<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <th style="width:20%">Field Name</th>
                <td>Field Value</td>
            </tr>
            <tr>
                <th>Field Name 2</th>
                <td>Field Value 2</td>
            </tr>

            <?php if($with_card == FALSE) : ?>
            <tr>
                <th>Status Data</th>
                <td>
                    <span class="label label-success">AKTIF</span>
                    <a href="" class="btn btn-xs btn-danger">Arsipkan/Jadikan Non-Aktif</a>
                </td>
            </tr>
            <tr>
                <th>Status Approve</th>
                <td>
                    <span class="label label-success">DISETUJUI</span>
                    <a href="" class="btn btn-xs btn-warning">Batalkan Persetujuan</a>
                </td>
            </tr>
            <?php endif; ?>

        </tbody>
    </table>
</div>