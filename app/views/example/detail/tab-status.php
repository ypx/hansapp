<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr>
                <th style="width:20%">Create By</th>
                <td>{create_by}</td>
            </tr>
            <tr>
                <th>Create Date</th>
                <td>{create_date}</td>
            </tr>
            <tr>
                <th>Modified By</th>
                <td>{mod_by}</td>
            </tr>
            <tr>
                <th>Modified Date</th>
                <td>{mod_date}</td>
            </tr>
        </tbody>
    </table>
</div>