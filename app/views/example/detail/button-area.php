<a href="<?php echo site_url( $this->cur . '/view') ?>" class="btn btn-default">
    <i class="fa fa-long-arrow-left"></i>
    Kembali
</a>
<a href="<?php echo site_url( $this->cur . '/form') ?>" class="btn btn-info">
    <i class="fa fa-pencil"></i>
    Update Data
</a>