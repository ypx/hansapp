<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a aria-expanded="true" href="#general" data-toggle="tab">General</a></li>
        <li><a aria-expanded="true" href="#child" data-toggle="tab">Daftar Data</a></li>
        <li><a aria-expanded="true" href="#status" data-toggle="tab">Status</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="general">
            <?php $this->load->view($this->cur . '/detail/tab-general'); ?>
        </div>
        <div class="tab-pane" id="child">
            <?php $this->load->view($this->cur . '/detail/tab-child'); ?>
        </div>
        <div class="tab-pane" id="status">
            <?php $this->load->view($this->cur . '/detail/tab-status'); ?>
        </div>
    </div>
</div>