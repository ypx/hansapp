<div class="box box-primary">
    <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="<?php echo default_img('avatar') ?>" alt="User profile picture">

        <h3 class="profile-username text-center">Display Name</h3>

        <p class="text-muted text-center">Description Data</p>
        <p class="text-center">
            <?php if (0): ?>
                <span class="label label-success text-center">Aktif</span>    
            <?php else: ?>
                <span class="label label-danger text-center">Tidak Aktif</span>    
            <?php endif ?>
            <?php if (0): ?>
                <span class="label label-success text-center">Disetujui</span>   
            <?php else: ?>
                <span class="label label-danger text-center">Menunggu Persetujuan</span>
            <?php endif ?>
            
            <!-- <span class="label label-danger text-center">Rejected</span> -->
            <!-- <span class="label label-success text-center">Approved</span> -->
            <!-- <span class="label label-success text-center">Aktif</span> -->
            
        </p>

        <a href="<?=base_url($this->cur . '/form/')?>" class="btn btn-default btn-block"><b>Edit Data</b></a>

        <?php if (1): ?>
            <a href="<?=base_url($this->cur.'/approve/')?>" class="btn btn-success btn-block"><b>Setujui</b></a>    
        <?php else: ?>
            <a href="<?=base_url($this->cur.'/unapproved/')?>" class="btn btn-danger btn-block"><b>Tidak Disetujui</b></a>    
        <?php endif ?>

        <?php if (0): ?>
            <a href="<?=base_url($this->cur.'/hapus/')?>" class="btn btn-danger btn-block"><b>Arsipkan/Soft Delete</b></a>    
        <?php else: ?>
            <a href="<?=base_url($this->cur.'/aktif/')?>" class="btn btn-info btn-block"><b>Aktifkan</b></a>    
        <?php endif ?>
        
        <a href="<?=base_url($this->cur)?>"  class="btn btn-default btn-block"><b>Kembali</b></a>
    </div>
<!-- /.box-body -->
</div>

