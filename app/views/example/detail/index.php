<?php $this->load->view('snippets/notification'); ?>

<div class="row">
    
    <?php if($with_card == TRUE) : ?>
    <div class="col-md-3">
        <?php $this->load->view( $this->cur . '/detail/left' ); ?>
    </div>
    <div class="col-md-9">
        <?php $this->load->view( $this->cur . '/detail/right' ) ?>
    <?php else : ?>
    <div class="col-md-12">
        <?php $this->load->view( $this->cur . '/detail/right' ) ?>
        <?php $this->load->view( $this->cur . '/detail/button-area' ) ?>
    <?php endif; ?>
        
    </div>
</div>
