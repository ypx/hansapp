<table class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th>Field Title 1</th>
            <th>Field Title 2</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php //foreach ($datas as $key => $data): ?>
        <tr>
            <td>Field One</td>
            <td>Field Two</td>
            <td>
                <?php if (1): ?>
                    <span class="label label-success">Disetujui</span>
                <?php else: ?>
                    <span class="label label-danger">Belum Disetujui</span>    
                <?php endif ?>
                <?php if (1): ?>
                    <span class="label label-success">Aktif</span>    
                <?php else: ?>
                    <span class="label label-danger">Tidak Aktif</span>
                <?php endif ?>
            </td>
            <td>
                <a href="<?php echo site_url($this->cur . "/detail") ?>" class="btn btn-xs btn-info">Detail</a>
                <a href="<?php echo site_url($this->cur . "/form") ?>" class="btn btn-xs btn-default">Update</a>
            </td>
        </tr>    
    <?php //endforeach; ?>
    </tbody>
</table>