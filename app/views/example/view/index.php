<?php $this->load->view('snippets/notification') ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Contoh</h3>
                <div class="box-tools">
                    <a href="<?php echo base_url('example/form') ?>" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i> Tambah Data
                    </a>
                </div>
            </div> <!-- /.box-header -->
            <div class="box-body">
                <?php $this->load->view($this->cur . '/view/table') ?>
            </div> <!-- /.box-body -->
        </div> <!-- /.box -->
    </div> <!-- /.col -->
</div>

<?php $this->load->view('snippets/notification') ?>