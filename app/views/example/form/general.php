<div class="box box-default">

    <div class="box-header with-border">
        <h3 class="box-title">Info General</h3>
    </div> <!-- /.box-header -->

    <div class="box-body">
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Text</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="text" name="field_name" value="">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <?php // if ($status == 'update'): ?>
                    <p class="help-block">
                        * Keterangan field form
                    </p>        
                <?php // endif ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Single Select</label>
            <div class="col-sm-10">
                <select name="" id="select_one" class="form-control select2">
                    <option value="">-- Pilih Opsi --</option>
                    <option value="">Ini</option>
                    <option value="">Contoh</option>
                    <option value="">Untuk</option>
                    <option value="">Memilih</option>
                    <option value="">Single</option>
                    <option value="">Option</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Multiple Select</label>
            <div class="col-sm-10">
                <select multiple name="" class="form-control select2">
                    <option value="">-- Pilih Opsi --</option>
                    <option value="">Ini</option>
                    <option value="">Contoh</option>
                    <option value="">Untuk</option>
                    <option value="">Memilih</option>
                    <option value="">Object</option>
                    <option value="">Option</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Datepicker</label>
            <div class="col-sm-10">
                <input type="text" class="form-control datepicker" value="">
            </div>
        </div>

        

        <div class="form-group">
            <label class="col-sm-2 control-label">Upload Foto</label>
            <div class="col-sm-10">
                
                <img src="http://placehold.it/200x200" alt="..." class="margin">    

                <input type="file" name="photo" class="form-control">
                <?php //if ($status == 'update'): ?>
                    <p class="help-block">
                        * Isi jika ingin mengubah foto
                    </p>
                <?php //endif ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Status Persetujuan</label>
            <div class="radio col-sm-10">
                <label>
                    <input type="radio" value="1" name="status" <?=(@$users[0]->status == 1) ? 'checked' : ''?>> <span class="label label-success">Setujui Sekarang</span>
                </label>
                <label>
                    <input type="radio" value="0" name="status" <?=(@$users[0]->status == 0) ? 'checked' : ''?>> <span class="label label-warning">Jangan Setujui Sekarang</span>
                </label>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Status Aktif</label>
            <div class="radio col-sm-10">
                <label>
                    <input type="radio" value="1" name="flag" <?=(@$users[0]->flag == 1) ? 'checked' : ''?>> <span class="label label-success">Aktifkan Sekarang</span>
                </label>
                <label>
                    <input type="radio" value="0" name="flag" <?=(@$users[0]->flag == 0) ? 'checked' : ''?>> <span class="label label-danger">Sembunyikan Dahulu</span>
                </label>
            </div>
        </div>

    </div>

    <!-- /.box-body -->
    <div class="box-footer">
        <a href="<?php echo base_url( $this->cur . '/view') ?>" class="btn btn-default">Batal & Kembali</a>
        <button type="submit" class="btn btn-info pull-right">Simpan</button>
    </div>
</div>