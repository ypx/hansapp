<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HANS_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $data['sitename'] = "Hans Ganteng";
        $this->load->vars($data);
        $this->load->model('M_global');
    }

    public function index()
    {
        
    }

    public function setCur($cur)
    {
        $data['cur'] = $cur;
        $this->load->vars($data);
    }

}
