<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends HANS_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->cur = 'dashboard';
    }

    public function index()
    {
        redirect( $this->cur . "/view" );
    }

    public function view()
    {
        $data['page_title']  = 'Dashboard';
        $data['subpage_title']     = 'Optimasi Penjadwalan Damping Mahasiswa Difabel Menggunakan Metode Algoritma Genetik';
        $this->template->views('dashboard/view/index', $data);
    }

}

/* End of file Dashboard.php */
/* Location: .//Users/yuripertamax/Repositories/Works/Digado/dashboard/app/controllers/Dashboard.php */