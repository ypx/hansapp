<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Example extends DGD_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->cur      = 'example';
        $this->table    = 'example';
    }

    public function index()
    {
        redirect($this->cur . '/view');
    }

    public function view()
    {
        $data['page_title']     = 'Contoh';
        $data['subpage_title']  = 'Contoh Halaman View';
        $this->template->views('example/view/index', $data);
    }

    public function detail()
    {
        $data['page_title']     = 'Contoh';
        $data['subpage_title']  = 'Contoh Halaman Detail';
        $data['with_card'] = TRUE; /* SET THIS IF YOU WANT TO CHANGE LAYOUT */
        $this->template->views('example/detail/index', $data);
    }

    public function form()
    {
        $data['page_title']     = 'Contoh';
        $data['subpage_title']  = 'Contoh Halaman Form';
        $data['act'] = $this->cur . "/view";
        $this->template->views('example/form/index', $data);
    }

    public function save_add()
    {
        if ($proses >= 0) {
            $this->session->set_flashdata('alert_msg', succ_msg('Admin berhasil diupdate'));
        } else {
            $this->session->set_flashdata('alert_msg', err_msg('Admin berhasil diupdate'));
        }
        # code...
    }

}

/* End of file Example.php */
/* Location: .//Users/yuripertamax/Repositories/Works/Rumah360/dashboard/app/controllers/Example.php */