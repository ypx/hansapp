<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends HANS_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->cur      = 'data';
        $this->load->model('Model_hitung', 'hitung');
        // $this->table    = 'example';
    }

    public function index()
    {
        redirect($this->cur . '/view');
    }

    public function view()
    {
        $data['page_title']     = 'Data';
        $data['subpage_title']  = 'Perhitungan Data';
        $this->session->set_flashdata('alert_msg', alert_msg('info', 'Data Masih Kosong', 'Silahkan klik tombol Input Data'));
        $this->template->views('data/view', $data);
    }

    public function generateRandomGen($count = null, $length = null)
    {
        header('Content-Type: application/json');
        $dataGen = array();
        $singleGen = "";
        for( $i = 1; $i <= $count; $i++ ) {
            for ( $j = 1; $j <= $length; $j++ ) { 
                $singleGen .= rand(1,3) . " ";
            }
            $singleDataGen[0] = "P" . $i;
            $singleDataGen[1] = trim($singleGen);
            array_push($dataGen, $singleDataGen);
            $singleGen = "";
        }
        $dataSetGenObject = new stdClass();
        $dataSetGenObject->draw = 1;
        $dataSetGenObject->recordsTotal = $count;
        $dataSetGenObject->recordFiltered = $count;
        $dataSetGenObject->data = $dataGen;

        // print_r($dataSetGenObject);
        // $this->input->set_cookie('populasi', $dataSetGenObject);
        echo json_encode($dataSetGenObject);
    }

    public function hitung()
    {
        // echo "<pre>";
        // print_r($this->input->post());

        $populasiCount = $this->input->post('populasiCount');
        $iterasiValue = $this->input->post('iterasiValue');
        $crValue = $this->input->post('crValue');
        $mrValue = $this->input->post('mrValue');
        $populasiValue = array();
        for($i = 1; $i <= $populasiCount; $i++) {
            $populasiValue['P'.$i] = explode(' ', $this->input->post('P'.$i));
        }

        $cr = $crValue;
        $mr = $mrValue;
        $iterasi = $iterasiValue;
        $populasi = $populasiValue;
        // echo ceil(0.6*4);
        // exit;
        $populasiCount = sizeof($populasi);

        $resultTable = array();

        for ($i=0; $i < $iterasi; $i++) { 
            $crossover = $this->hitung->get_crossover($populasi, $cr, $populasiCount);
            $tampil_crossover = $this->tampil_crossover($populasi, $crossover, $cr, $populasiCount);
            $resultTable['crossover'][$i] = $tampil_crossover;
            $jml_crossover = ceil($cr*$populasiCount);
            for ($x=1; $x <= $jml_crossover; $x++) { 
                $populasi['c'.$x] = $crossover[$x];
            }
            $mutasi = $this->hitung->get_mutasi($populasi,$mr, $populasiCount);
            $tampil_mutasi = $this->tampil_mutasi($populasi,$mutasi,$mr, $populasiCount);
            $resultTable['mutasi'][$i] = $tampil_mutasi;
            $c_ke = count($populasi)-$populasiCount;
            $jml_mutasi = ceil($mr*$populasiCount);
            for ($y=1; $y <= $jml_mutasi; $y++) { 
                $c = $y+$c_ke;
                $populasi['c'.$c] = $mutasi[$y];
            }
            $seleksi = $this->hitung->get_seleksi($populasi, $populasiCount);
            $tampil_seleksi = $this->tampil_seleksi($seleksi, $populasiCount);
            $resultTable['seleksi'][$i] = $tampil_seleksi;
            
            unset($populasi);
            for ($n=1; $n <= $populasiCount ; $n++) { 
                $populasi['p'.$n] = $seleksi[$n-1][$seleksi[$n-1]['nama']];
            }
        }

        // echo "<print>";
        // print_r($tampil_crossover);
        $this->session->set_flashdata('alert_msg', alert_msg('info', 'Hasil Data', 'Jangan refresh halaman ini'));
        $this->session->set_flashdata('seleksi_table', $tampil_seleksi);
        $this->session->set_flashdata('crossover_table', $tampil_crossover);
        $this->session->set_flashdata('mutasi_table', $tampil_mutasi);

        redirect('data/view');
    }

    public function tampil_crossover($populasi,$crossover,$cr, $populasiCount)
    {
        $jml = ceil($cr* $populasiCount);
        for ($i=1; $i <= $jml; $i++) { 
            $parent[$i][1] = $crossover['parent'.$i][1];
            $parent[$i][2] = $crossover['parent'.$i][2];
            unset($crossover['parent'.$i]);
        }

        $list = '';
        $num=1;
        foreach ($crossover as $key => $value) {
            $parent_1 = implode(' ', $populasi[$parent[$num][1]]);
            $parent_2 = implode(' ', $populasi[$parent[$num][2]]);
            $child = implode(' ', $crossover[$num]);
            $list .=
                '<h3 class="form-section">Crossover '.$parent[$num][1].' dan '.$parent[$num][2].'</h3>
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered table-advance">
                          <thead>
                              <tr>
                                  <th>Nama Individu</th>
                                  <th>Nilai</th>
                              </tr>
                          </thead>
                          <tbody>
                                <tr>
                                    <td>'.$parent[$num][1].'</td>
                                    <td>'.$parent_1.'</td>
                                </tr>
                                <tr>
                                    <td>'.$parent[$num][2].'</td>
                                    <td>'.$parent_2.'</td>
                                </tr>
                                <tr>
                                    <td>c'.$num.'</td>
                                    <td>'.$child.'</td>
                                </tr>
                          </tbody>
                      </table>
                  </div>';
            $num++;
        }
        return $list;
    }

    public function tampil_mutasi($populasi,$mutasi,$mr, $populasiCount)
    {
        $jml = ceil($mr*$populasiCount);
        for ($i=1; $i <= $jml; $i++) { 
            $parent[$i] = $mutasi['parent'.$i];
            unset($mutasi['parent'.$i]);
        }

        $c_ke = count($populasi)-$populasiCount;
        // print_r($mutasi);exit;

        $list = '';
        $num=1;
        foreach ($mutasi as $key => $value) {
            $isi_parent = implode(' ', $populasi[$parent[$num]]);
            $child = implode(' ', $mutasi[$num]);
            $nama_child = $num+$c_ke;
            $list .=
                '<h3 class="form-section">Mutasi '.$parent[$num].'</h3>
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered table-advance">
                          <thead>
                              <tr>
                                  <th>Nama Individu</th>
                                  <th>Nilai</th>
                              </tr>
                          </thead>
                          <tbody>
                                <tr>
                                    <td>'.$parent[$num].'</td>
                                    <td>'.$isi_parent.'</td>
                                </tr>
                                <tr>
                                    <td>c'.$nama_child.'</td>
                                    <td>'.$child.'</td>
                                </tr>
                          </tbody>
                      </table>
                  </div>';
            $num++;
        }

        return $list;
    }

    public function tampil_seleksi($seleksi)
    {
        $list = '';
        $num=1;
        $list .=
            '<h3 class="form-section">Hasil Seleksi</h3>
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered table-advance">
                          <thead>
                              <tr>
                                  <th>No</th>
                                  <th>Populasi</th>
                                  <th>Gen</th>
                                  <th>Plg.1</th>
                                  <th>Plg.2</th>
                                  <th>Plg.3</th>
                                  <th>Perhitungan</th>
                                  <th>Total Plg.</th>
                                  <th>Nilai Fitness</th>
                              </tr>
                          </thead>
                          <tbody>';
        foreach ($seleksi as $key => $value) {
            if ($value['nilai']!=0) {
                $nana = 10000/$value['nilai'];
            } else {
                $nana = 1000;
            }
            $list .=
            '<tr>
                    <td>'.$num.'</td>
                    <td>'.$value["nama"].'</td>
                    <td>'.implode(' ', $value[$value["nama"]]).'</td>
                    <td>'.$value["pelanggaran 1"].'</td>
                    <td>'.$value["pelanggaran 2"].'</td>
                    <td> 0 </td>
                    <td>('.$value["pelanggaran 1"].' x 50) + ('.$value["pelanggaran 2"].' x 20) + (0 X 2)</td>
                    <td>'.$value["nilai"].'</td>
                    <td>'.$nana.'</td>
                </tr>';
            $num++;
            if ($num==5) {
                break;
            }
        }
        $list .= '</tbody>
                </table>
                </div>';
        return $list;
    }

}

/* End of file Example.php */
/* Location: .//Users/yuripertamax/Repositories/Works/Rumah360/dashboard/app/controllers/Example.php */