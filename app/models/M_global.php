<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_global extends CI_Model {

    public function getList($table, $where = '' )
    {
        if ($where) {
            $this->db->where($where);
        }
        return $this->db->get($table)->result();
    }   

    public function getListJoin($select, $table, $table_join, $where = '', $order = '', $sort = '' )
    {
        $this->db->select($select);

        $this->db->from($table);

        for ($i=0; $i < count($table_join) ; $i++) { 
            $this->db->join($table_join[$i]['table'], $table_join[$i]['where'] , 'left');
        }

        if ($where) {
            $this->db->where($where);
        }

        if ($order) {
            $this->db->order_by($order, $sort);
        }
        
        return $this->db->get()->result();
    }

    public function insert($table, $param)
    {
        $this->db->insert($table, $param);

        return $this->db->insert_id();
    }

    public function update($table, $set, $where)
    {
        $this->db->where($where);
        $this->db->update($table, $set);

        return $this->db->affected_rows();
    }

    public function delete($table, $where)
    {
        $this->db->where($where);
        $this->db->delete($table);

        return $this->db->affected_rows();
    }

}

/* End of file M_golbal.php */
/* Location: .//Users/toni/Documents/public_html/rumah360/dashboard/app/models/M_golbal.php */