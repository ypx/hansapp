<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->userdb = $this->load->database('user', TRUE);
        $this->userlog = $this->load->database('log', TRUE);
    }    

    public function get( $username = null )
    {
        $sql = "select * from user_data";
        $query = $this->userdb->query($sql);
        return $query->result();
    }

    public function getLog( $username = null )
    {
        $query = $this->userlog->get('log_data');
        return $query->result();
        # code...
    }

}

/* End of file Model_user.php */
/* Location: .//Users/yuripertamax/Repositories/Works/Digado/dashboard/app/models/Model_user.php */