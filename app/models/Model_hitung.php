<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_hitung extends CI_Model {

    public function get_crossover($populasi,$cr,$populasiCount)
    {
        $jml = ceil($cr*$populasiCount);
        for ($n=1; $n <= $jml ; $n++) { 
            $random = array_rand($populasi,2);
            $parent1 = $populasi[$random[0]];
            $parent2 = $populasi[$random[1]];

            $num = count($parent1)-1;
            $cut_point = rand(0,$num);

            for ($i=0; $i < $cut_point; $i++) { 
                $cross[$n][$i] = $parent1[$i];
            }

            for ($i=$cut_point; $i <= $num; $i++) { 
                $cross[$n][$i] = $parent2[$i];
            }

            $cross['parent'.$n][1] = $random[0];
            $cross['parent'.$n][2] = $random[1];
        }
        return $cross;
    } 

    public function get_mutasi($populasi,$mr,$populasiCount)
    {
        $jml = ceil($mr*$populasiCount);
        for ($n=1; $n <= $jml ; $n++) { 
            $random = array_rand($populasi,1);
            $parent = $populasi[$random];

            $num = count($parent)-1;
            $random1 = rand(0,$num);
            while( in_array( ($random2 = rand(1,$num)), array($random1) ) );

            $a = $parent[$random1];
            $b = $parent[$random2];

            $mutasi[$n] = $parent;
            $mutasi[$n][$random1]=$b;
            $mutasi[$n][$random2]=$a;

            $mutasi['parent'.$n] = $random;
        }
        return $mutasi;
    }

    function sortByNilai($a, $b) {
        return $a['nilai'] - $b['nilai'];
    }

    public function get_seleksi($populasi)
    {
        foreach ($populasi as $key => $value) {
            $nilai[$key] = $this->get_nilai($key,$value);
        }        

        usort($nilai, array($this, 'sortByNilai'));

        return $nilai;
    }

    public function get_nilai($populasi,$value)
    {
        $pendamping1= $this->get_pelanggaran($populasi.'1');
        $pendamping2= $this->get_pelanggaran($populasi.'2');
        $pendamping3= $this->get_pelanggaran($populasi.'3');

        $jml = count($value);
        $pelanggaran1 = 0;
        $pelanggaran2 = 0;

        // print_r($value);exit;

        for ($i=0; $i < $jml; $i++) { 
            if ($value[$i]==1) {
                if ($pendamping1[$i]==1) {
                    $pelanggaran1 +=1;
                }
                if ($pendamping1[$i]==2) {
                    $pelanggaran2 +=1;
                }
            }
            if ($value[$i]==2) {
                if ($pendamping2[$i]==1) {
                    $pelanggaran1 +=1;
                }
                if ($pendamping2[$i]==2) {
                    $pelanggaran2 +=1;
                }
            }
            if ($value[$i]==3) {
                if ($pendamping3[$i]==1) {
                    $pelanggaran1 +=1;
                }
                if ($pendamping3[$i]==2) {
                    $pelanggaran2 +=1;
                }
            }
        }

        $nilai = ($pelanggaran1*50)+($pelanggaran2*20);

        $hasil = array('nama' => $populasi, $populasi => $value,'pelanggaran 1'=>$pelanggaran1,'pelanggaran 2'=>$pelanggaran2,'nilai'=>$nilai);
        return $hasil;
    }

    public function get_pelanggaran($populasi)
    {
        $p11 = array(1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0);
        $p12 = array(0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0);
        $p13 = array(0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1);
        $p21 = array(1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0);
        $p22 = array(0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0);
        $p23 = array(0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
        $p31 = array(1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0);
        $p32 = array(0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0);
        $p33 = array(0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
        $p41 = array(1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1);
        $p42 = array(0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0);
        $p43 = array(0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0);
        $c11 = array(1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0);
        $c12 = array(0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0);
        $c13 = array(0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1);
        $c21 = array(1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0);
        $c22 = array(0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0);
        $c23 = array(0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1);
        $c31 = array(1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0);
        $c32 = array(0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0);
        $c33 = array(0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1);
        $c41 = array(1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0);
        $c42 = array(0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0);
        $c43 = array(0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1);
        switch ($populasi) {
            case 'p11':
                return $p11;
                break;
            case 'p12':
                return $p12;
                break;
            case 'p13':
                return $p13;
                break;
            case 'p21':
                return $p21;
                break;
            case 'p22':
                return $p22;
                break;
            case 'p23':
                return $p23;
                break;
            case 'p31':
                return $p31;
                break;
            case 'p32':
                return $p32;
                break;
            case 'p33':
                return $p33;
                break;
            case 'p41':
                return $p41;
                break;
            case 'p42':
                return $p42;
                break;
            case 'p43':
                return $p43;
                break;
            case 'c11':
                return $c11;
                break;
            case 'c12':
                return $c12;
                break;
            case 'c13':
                return $c13;
                break;
            case 'c21':
                return $c21;
                break;
            case 'c22':
                return $c22;
                break;
            case 'c23':
                return $c23;
                break;
            case 'c31':
                return $c31;
                break;
            case 'c32':
                return $c32;
                break;
            case 'c33':
                return $c33;
                break;
            case 'c41':
                return $c41;
                break;
            case 'c42':
                return $c42;
                break;
            case 'c43':
                return $c43;
                break;
        }
    }

}

/* End of file Model_hitung.php */
/* Location: .//Users/yuripertamax/Repositories/Misc/Hans/app/models/Model_hitung.php */