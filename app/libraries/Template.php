<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Template {

    protected $_ci;

    function __construct() {
        $this->_ci = &get_instance();
    }

    function views( $template = NULL, $data = NULL ) {
        if( $template != NULL )
        $data['_body']      = $this->_ci->load->view($template, $data, TRUE);
        $data['_css']       = $this->_ci->load->view('_layouts/_css', $data, TRUE);
        $data['_js']        = $this->_ci->load->view('_layouts/_js', $data, TRUE);
        $data['_header']    = $this->_ci->load->view('_layouts/_header', $data, TRUE);
        $data['_footer']    = $this->_ci->load->view('_layouts/_footer', $data, TRUE);
        $data['_head']      = $this->_ci->load->view('_layouts/_head', $data, TRUE);
        $data['_foot']      = $this->_ci->load->view('_layouts/_foot', $data, TRUE);
        $data['_menu']      = $this->_ci->load->view('_layouts/_menu', $data, TRUE);
        echo $data['_template']  = $this->_ci->load->view('_layouts/_template', $data, TRUE);
    }

    function auth_views( $template = NULL, $data = NULL ) {
        if( $template != NULL )
        $data['_body']      = $this->_ci->load->view($template, $data, TRUE);
        $data['_css']       = $this->_ci->load->view('_layouts/_css', $data, TRUE);
        $data['_js']        = $this->_ci->load->view('_layouts/_js', $data, TRUE);
        echo $data['_template']  = $this->_ci->load->view('_layouts/_auth', $data, TRUE);
    } 

}

?>
