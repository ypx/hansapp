<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('vendor_url'))
{
    function vendor_url( $libraries = null )
    {
        $url = base_url() . "vendor/" . $libraries;
        return $url;
    }
}