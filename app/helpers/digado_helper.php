<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('assets_url'))
{
    function assets_url( $type = null, $path = null )
    {
        $url = base_url() . "assets/" . $type . "/" . $path;
        return $url;
    }
}

if (!function_exists('get_uploads'))
{
    function get_uploads( $type = null, $cur = null, $file = null, $option = null )
    {   
        if($option == true) {
            $url = FCPATH . "uploads/" . $type . "/" . $cur . "/" . $file;
        } else {
            $url = base_url() . "uploads/" . $type . "/" . $cur . "/" . $file;
        }
        return $url;
    }
}

if (!function_exists('alert_msg'))
{
    function alert_msg( $type = null, $title = null, $msg = null, $close = true )
    {   
        $type = $type ? $type : 'default';
        $title = $title ? $title : 'Digado Notification';
        $msg = $msg ? $msg : 'Halo, ayo join dan ngobrol dengan Digado';
        
        $html = "<div class='alert alert-{$type} alert-block'>";
        if($close == true) {
            $html .= "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
        }
        $html .= "<h4 class='alert-heading'>{$title}</h4>{$msg}</div>";

        return $html;
    }
}

if (!function_exists('debug')) 
{
    function debug( $str = null, $exit = false )
    {
        $str = $str ? $str : phpinfo(); 
        echo "<pre>";
        alert_msg('info', 'DEBUG', print_r($str));
        echo "</pre>";
        if($exit == true) exit();
        # code...
    }
}

if (!function_exists('default_img'))
{
    function default_img( $type = null )
    {
        switch ( $type ) {
            case 'avatar':
                $url = assets_url('img', 'default/avatar-200x200.png');
                break;

            default:
                # code...
                break;
        }
        return $url;
    }
}

function menu_status($type = 'NOTYET')
{
    switch ($type) {
        case 'WIP':
            $emotion = 'yellow';
            $icon = 'minus-square';
            break;
        case 'NOTYET':
            $emotion = 'red';
            $icon = 'exclamation-circle';
            break;
        case 'DONE':
            $emotion = 'green';
            $icon = 'check';
            break;
        default:
            $emotion = 'red';
            $icon = 'exclamation-circle';
            break;
    }
    if(ENV == 'develop' or ENV == 'test') {
        echo "<i class='text-{$emotion} fa fa-{$icon}'></i>";
    } else {
        echo FALSE;
    }
}

function branch_name()
{
    $stringfromfile = file('.git/HEAD', FILE_USE_INCLUDE_PATH);
    $firstLine = $stringfromfile[0]; //get the string from the array
    $explodedstring = explode("/", $firstLine, 3); //seperate out by the "/" in the string
    return $branchname = trim($explodedstring[2]);
    # code...
}

function sendSMS( $number = null, $msg = null )
{
    $dataSMS = array(
        'userkey'   => '',
        'passkey'   => '',
        'nohp'      => $number,
        'pesan'     => $msg
    );
    
    $dataSMSquery = http_build_query($dataSMS);
    $smsurl = "https://reguler.zenziva.net/apps/smsapi.php?{$dataSMSquery}";
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $smsurl); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch); 
    curl_close($ch); 
}

function date_format_db($value='', $tipe)
{
    # 12/02/1900

    if ($tipe == 'db') {
        $str = explode('/', $value);

        if (count($str) > 1) {
            $date = $str[2].'-'.$str[0].'-'.$str[1];

        }else{
            $date = '';
        }

        return $date;
    } else if ($tipe == 'web') {
        # 1990-03-24
        $str = explode('-', $value);

        if (count($str) > 1) {
            $date = @$str[2].'/'.@$str[1].'/'.$str[0];

        }else{
            $date = '';
        }

        return @$date;
        
    }
    
    
}

